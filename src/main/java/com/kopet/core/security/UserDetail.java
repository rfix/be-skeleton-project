package com.kopet.core.security;

import com.kopet.core.constant.RoleEnumConstant;
import com.kopet.core.entity.users.Role;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

public class UserDetail {
    public static UserDetailsImpl currentUser() {
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static List<RoleEnumConstant> listRole() {
        return currentUser().getUsers().getRoles().stream().map(Role::getName).collect(Collectors.toList());
    }

    public static boolean isAdmin() {
        return listRole().contains(RoleEnumConstant.ROLE_ADMIN);
    }

    public static String getUsername(){
        return currentUser().getUsername();
    }
}
