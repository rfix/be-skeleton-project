package com.kopet.core.security.jwt;

import com.kopet.core.security.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${app.jwt.secret}")
    private String jwtSecret;

    @Value("${app.jwt.expiration.ms}")
    private int jwtExpirationMs;

    public String refreshToken(String token) {
        String refreshedToken = null;
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
            claims.setExpiration(generateExpirationDate());
            refreshedToken = Jwts.builder()
                    .setClaims(claims)
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact();
        } catch (JwtException | IllegalArgumentException e) {
            logger.error("JWT Token is expired or invalid");
        }
        return refreshedToken;
    }

    public String generateToken(Authentication authentication) {
        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        return jwtBuilder(userPrincipal.getUsername());
    }

    public String jwtBuilder(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(generateIssuedAtDate())
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            logger.error("JWT Token is expired or invalid");
            return false;
        }
    }

    public String generateRefreshToken() {
        return UUID.randomUUID().toString();
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + jwtExpirationMs);
    }

    private Date generateIssuedAtDate() {
        return new Date(System.currentTimeMillis());
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }
}
