package com.kopet.core.security;

import com.kopet.core.exception.NotFoundException;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.repository.UserAuthRepository;
import com.kopet.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	UserAuthRepository userAuthRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UserAuth> userAuth = userAuthRepository.findByEmailOrUsername(username);
		if (!userAuth.isPresent()) {
			throw new NotFoundException(username);
		}
		return UserDetailsImpl.build(userAuth.get());
	}

}
