package com.kopet.core.controller;

import com.kopet.core.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/info")
public class InfoController {

    @Autowired
    BuildProperties buildProperties;

    @GetMapping
    public ResponseEntity<?> info() {
        return ResponseEntity.ok(buildProperties);
    }

    @GetMapping(value = "test")
    public ResponseEntity<?> test(@RequestParam(name = "value") Integer value) {
        return ResponseEntity.ok(Helper.convertIntToString(value));
    }

}
