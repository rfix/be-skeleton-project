package com.kopet.core.controller;

import com.kopet.core.constant.RoleEnumConstant;
import com.kopet.core.entity.users.Role;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.entity.users.Users;
import com.kopet.core.exception.BadRequestException;
import com.kopet.core.payload.request.LoginRequest;
import com.kopet.core.payload.request.SignupRequest;
import com.kopet.core.payload.response.BaseResponse;
import com.kopet.core.payload.response.JwtResponse;
import com.kopet.core.payload.response.JwtTokenResponse;
import com.kopet.core.payload.response.MessageResponse;
import com.kopet.core.repository.RoleRepository;
import com.kopet.core.repository.UserAuthRepository;
import com.kopet.core.repository.UserRepository;
import com.kopet.core.security.UserDetailsImpl;
import com.kopet.core.security.jwt.JwtUtils;
import com.kopet.core.service.users.UserService;
import com.kopet.core.exception.ThrowCustomAuthenticationError;
import com.kopet.core.payload.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserAuthRepository userAuthRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        UserAuth userAuth = userDetails.getUsers();

        userAuth.setRefreshToken(jwtUtils.generateRefreshToken());
        userAuth.setLoginAt(new Date());
        userAuth.setIsActive(true);
        userAuthRepository.save(userAuth);

        return ResponseEntity.ok(new BaseResponse(new JwtResponse(
                jwt,
                userAuth.getRefreshToken(),
                userDetails.getUsername(),
                userDetails.getUsers(),
                roles)));
    }

    @GetMapping(value = "/refresh-token")
    public ResponseEntity<?> refreshToken(HttpServletResponse response, HttpServletRequest request) throws IOException {
        try {
            String refreshToken = request.getHeader("UserSessionToken");
            UserAuth userAuth = userService.getUserByRefreshToken(refreshToken);
            String jwt = jwtUtils.jwtBuilder(userAuth.getUsername());

            return ResponseEntity.ok(new JwtTokenResponse(jwt, refreshToken));
        } catch (Exception e) {
            ThrowCustomAuthenticationError.invalidAccessToken(response, request);
            return null;
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userAuthRepository.existsByUsername(signUpRequest.getNrp())) {
            throw new BadRequestException("Error: Username is already taken!");
        }

        UserAuth userAuth = new UserAuth(signUpRequest.getNrp(), null, encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(RoleEnumConstant.ROLE_KARYAWAN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                if ("admin".equals(role)) {
                    Role adminRole = roleRepository.findByName(RoleEnumConstant.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(adminRole);
                } else {
                    Role userRole = roleRepository.findByName(RoleEnumConstant.ROLE_SISWA)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(userRole);
                }
            });
        }

        userAuth.setRoles(roles);
        UserAuth rowUser = userAuthRepository.save(userAuth);
        Users users = new Users();
        users.setName(signUpRequest.getName());
        users.setAddress(signUpRequest.getAddress());
        users.setDateOfBirth(signUpRequest.getBirthDate());
        users.setUserAuth(rowUser);
        users.setUpdatedAt(new Date());
        users.setCreatedAt(new Date());
        userRepository.save(users);

        return ResponseEntity.ok(new MessageResponse(true, "User registered successfully!"));
    }

}
