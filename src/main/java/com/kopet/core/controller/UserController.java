package com.kopet.core.controller;

import com.kopet.core.constant.FileTypeConstant;
import com.kopet.core.constant.MessageConstant;
import com.kopet.core.entity.users.Users;
import com.kopet.core.payload.request.ChangePasswordRequest;
import com.kopet.core.payload.request.users.UserRequest;
import com.kopet.core.payload.response.BaseResponse;
import com.kopet.core.payload.response.MessageResponse;
import com.kopet.core.service.users.ReadExcel;
import com.kopet.core.service.users.UserRelationsService;
import com.kopet.core.service.users.UserService;
import com.kopet.core.utils.Helper;
import com.kopet.core.exception.UnProcessEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    UserRelationsService userRelationsService;

    @GetMapping
    public ResponseEntity<?> getUser() {
        return ResponseEntity.ok(new BaseResponse(userService.showUsers()));
    }

    @PutMapping
    public ResponseEntity<?> updateData(@RequestBody UserRequest request) {
        return ResponseEntity.ok(new BaseResponse(userService.updateData(request)));
    }

    @PutMapping(value = "/change-password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest request) {
        userService.changePassword(request);
        return ResponseEntity.ok(new BaseResponse(MessageConstant.SUCCESS));
    }

    @PostMapping("/upload")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        if (ReadExcel.hasExcelFormat(file)) {
            try {
//                userService.save(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(true, message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(false, message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(false, message));
    }

    @GetMapping("/template")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<?> downloadTemplate() {
        try {
            InputStream template = Helper.getFileTemplateFromPathResourceFolder("bulk-user.xlsx");
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename = \"bulk-user.xlsx\"");
            headers.add(HttpHeaders.CONTENT_TYPE, FileTypeConstant.EXCEL);
            assert template != null;
            return ResponseEntity.ok().headers(headers).body(new InputStreamResource(template));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping(value = "/reset-password")
    public ResponseEntity<?> resetPassword(@RequestParam(value = "username") String username) {
        userService.resetPassword(username);
        return ResponseEntity.ok(new BaseResponse(MessageConstant.SUCCESS_UPDATED, true));
    }

    @GetMapping("/find")
    public ResponseEntity<?> findUser(@RequestParam(name = "name", required = false) String name,  Pageable page) {
        try {
            return new ResponseEntity<>(userService.findUser(Helper.convertEmptyToNull(name), page), HttpStatus.OK);
        } catch (Exception e) {
            throw new UnProcessEntityException(MessageConstant.SWW);
        }
    }

    @GetMapping("/findStudent")
    public ResponseEntity<?> findStudent(@RequestParam(name = "name", required = false) String name,  Pageable page) {
        try {
            return new ResponseEntity<>(userService.findStudent(Helper.convertEmptyToNull(name), page), HttpStatus.OK);
        } catch (Exception e) {
            throw new UnProcessEntityException(MessageConstant.SWW);
        }
    }

    @GetMapping("/subordinate")
    public ResponseEntity<?> getSubordinate(@RequestParam(name = "date", required = false) Date date) {
        return ResponseEntity.ok(new BaseResponse(userRelationsService.findSubordinate(date)));
    }

    @PutMapping(value = "/updateUser/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody Users request) {
        return ResponseEntity.ok(new BaseResponse(userService.updateUser(id, request), MessageConstant.SUCCESS_UPDATED));
    }
}
