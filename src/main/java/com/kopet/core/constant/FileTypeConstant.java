package com.kopet.core.constant;

public class FileTypeConstant {
    public static final String TXT = "txt",
            EXCEL = "application/vnd.ms-excel",
            ZIP = "application/zip",
            PDF = "application/pdf",
            DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            DOC = "application/msword",
            SHEET = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
}
