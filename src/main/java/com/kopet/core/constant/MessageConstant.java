package com.kopet.core.constant;


public class MessageConstant {
    public static final String SUCCESS = "successfully",
            SUCCESS_DELETED = "delete data " + SUCCESS,
            SUCCESS_UPDATED = "update data " + SUCCESS,
            SUCCESS_CREATE = "created data " + SUCCESS,
            SWW = "Something when wrong!",
            INVALID_DEVICE = "Please check your device",
            RESTRICT_AUTHENTICATION = "Cannot access this API",
            INVALID_REFRESH_TOKEN = "Please check your refresh token",
            SESSION_TIME_OUT = "Session time out!",
            BAD_CREDENTIALS = "Bad credentials";

    public static String UNAUTHORIZED = "Unauthorized", BAD_REQUEST = "Bad Request";

    public static String createMessageClass(Class<?> clazz) {
        return clazz.getSimpleName() + " " + SUCCESS_CREATE;
    }

    public static String updateMessageClass(Class<?> clazz) {
        return clazz.getSimpleName() + " " + SUCCESS_UPDATED;
    }

    public static String deleteMessageClass(Class<?> clazz) {
        return clazz.getSimpleName() + " " + SUCCESS_DELETED;
    }
}
