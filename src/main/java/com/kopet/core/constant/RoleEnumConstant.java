package com.kopet.core.constant;

public enum RoleEnumConstant {
	ROLE_SISWA,
    ROLE_GURU,
    ROLE_ADMIN,
    ROLE_KARYAWAN
}
