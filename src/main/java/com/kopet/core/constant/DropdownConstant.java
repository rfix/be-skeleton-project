package com.kopet.core.constant;

public class DropdownConstant {
    public final static String PENALTY_LOGIN = "penalty_login",
            PENALTY_LOGOUT = "penalty_logout",
            PENALTY_3 = "penalty3",
            PENALTY_0 = "penalty0",
            PENALTY_4 = "penalty4",
            CHECK_IN = "check_in",
            CHECK_OUT = "check_out",
            CHECK_IN_OUT = "check_in_out",
            PENALTY_KEY = "penalty_key",
            PENALTY = "penalty";
}
