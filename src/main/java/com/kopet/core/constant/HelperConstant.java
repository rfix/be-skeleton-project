package com.kopet.core.constant;

public class HelperConstant {
    public final static int ONE = 1, THREE = 3;

    public final static String CREATE = "create",
            UPDATE = "update",
            DELETE = "delete";
}
