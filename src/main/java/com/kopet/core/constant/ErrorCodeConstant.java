package com.kopet.core.constant;

public class ErrorCodeConstant {
    public static final String UNAUTHORIZED_SESSION = "E4011",
            UNAUTHORIZED_REFRESH_TOKEN = "E4012",
            BAD_REQUEST_CREDENTIAL = "E4001",
            BAD_REQUEST_BODY = "E4002",
            BAD_REQUEST_PARAMS = "E4003";
}
