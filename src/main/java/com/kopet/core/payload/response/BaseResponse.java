package com.kopet.core.payload.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BaseResponse extends GeneraleResponse {
    private Object data;

    public BaseResponse() {}

    public BaseResponse(Object data) {
        this.data = data;
    }

    public BaseResponse(String message) {
        this.message = message;
    }

    public BaseResponse(String message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public BaseResponse(Object data, String message) {
        this.message = message;
        this.data = data;
    }

    public BaseResponse(Object data, boolean status) {
        this.data = data;
        this.status = status;
    }

    public BaseResponse(Object data, boolean status, String message, String statusCode) {
        this.data = data;
        this.message = message;
        this.status = status;
        this.statusCode = statusCode;
    }
}
