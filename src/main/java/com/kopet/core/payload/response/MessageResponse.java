package com.kopet.core.payload.response;

import lombok.Data;

import java.util.Date;

@Data
public class MessageResponse {
	private Boolean status;
	private String message, error;
	private Date timestamp = new Date();

	public MessageResponse(Boolean status, String message) {
		this.message = message;
		this.status = status;
	}

	public MessageResponse(Boolean status, String message, String error) {
		this.message = message;
		this.status = status;
		this.error = error;
	}

}
