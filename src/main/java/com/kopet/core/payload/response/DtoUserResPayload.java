package com.kopet.core.payload.response;

import com.kopet.core.entity.users.Users;

/**
 *
 * @author rofiq
 */
public class DtoUserResPayload {

	public boolean success;
	public Long userId;
	public Users user;
	
	public static DtoUserResPayload createObject(Users usr, boolean  status) {
		DtoUserResPayload dto = new DtoUserResPayload();
		dto.success = status;
		dto.userId = usr.getId();
		dto.user = usr;
		return dto;
	}
	
	public static DtoUserResPayload create(Long userId, boolean status) {
		DtoUserResPayload dto = new DtoUserResPayload();
		dto.success = status;
		dto.userId = userId;
		return dto;
	}
	
}
