package com.kopet.core.payload.response;

import com.kopet.core.entity.users.UserAuth;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class JwtResponse extends JwtTokenResponse {
    private String username;
    private UserAuth user;
    private List<String> roles;

    public JwtResponse(String accessToken, String refreshToken, String username, UserAuth user, List<String> roles) {
        super(accessToken, refreshToken);
        this.username = username;
        this.user = user;
        this.roles = roles;
    }
}
