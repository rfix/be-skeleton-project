package com.kopet.core.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kopet.core.dto.UserParentDTO;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.entity.users.Users;
import lombok.Data;

import java.util.Date;

@Data
public class UserResponse {
    private Users users;

    private UserAuth userAuth;

    private UserParentDTO userParent;

    private String flaggingPenaltyLogin, flaggingPenaltyLogout;

    private Boolean confirmCheckIn, confirmCheckOut, isCheckIn, isCheckOut;

    @JsonFormat(pattern = "yyyy-MM-dd 16:30")
    private Date logoutTime = new Date();
}
