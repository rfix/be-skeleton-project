package com.kopet.core.payload.response;

import lombok.Data;

@Data
public class JwtTokenResponse {
    String accessToken, refreshToken;

    public JwtTokenResponse() {}

    public JwtTokenResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
