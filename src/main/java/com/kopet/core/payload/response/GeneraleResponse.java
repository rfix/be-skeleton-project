package com.kopet.core.payload.response;

import com.kopet.core.constant.MessageConstant;
import com.kopet.core.utils.RequestFetcher;
import lombok.Data;

import java.util.Date;

@Data
public class GeneraleResponse {
    Date timestamp = new Date();

    String message = MessageConstant.SUCCESS,
            statusCode;

    String path = RequestFetcher.getCurrentRequest().getRequestURI();

    boolean status = true;
}
