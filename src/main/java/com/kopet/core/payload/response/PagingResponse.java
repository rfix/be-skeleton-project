package com.kopet.core.payload.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.Page;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class PagingResponse  extends GeneraleResponse {

    public List<?> content;
    public Integer currentPage;
    public Long totalItems;
    public Integer totalPages;
    public Integer totalRow;
    public Object data;

    public PagingResponse(Page<?> page) {
        this.content = page.getContent();
        this.currentPage = page.getNumber();
        this.totalItems = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.totalRow = page.getSize();
    }

    public PagingResponse(Page<?> page, Object data) {
        this.content = page.getContent();
        this.currentPage = page.getNumber();
        this.totalItems = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.totalRow = page.getSize();
        this.data = data;
    }

    public PagingResponse(List<?> content, Integer currentPage, Long totalItems, Integer totalPages, Integer totalRow) {
        this.content = content;
        this.currentPage = currentPage;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.totalRow = totalRow;
    }
}
