package com.kopet.core.payload.request;

import lombok.Data;

@Data
public class ChangePasswordRequest {
    private String password;
}
