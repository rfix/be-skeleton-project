package com.kopet.core.payload.request.users;

import com.kopet.core.entity.users.Users;
import lombok.Data;

@Data
public class UserRequest {
    private Users profile;

    private String password, email;

    private Long userParentId;
}
