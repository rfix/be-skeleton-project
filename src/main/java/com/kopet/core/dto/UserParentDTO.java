package com.kopet.core.dto;

import com.kopet.core.entity.users.Users;
import lombok.Data;

@Data
public class UserParentDTO {
    private Long id;

    private String name;

    public UserParentDTO() {}

    public UserParentDTO(Users users) {
        this.id = users.getId();
        this.name = users.getName();
    }
}
