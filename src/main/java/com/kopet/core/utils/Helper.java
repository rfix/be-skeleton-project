package com.kopet.core.utils;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {

    public static String convertEmptyToNull(String s) {
        if (s != null && s.trim().isEmpty()) {
            return null;
        }
        return s;
    }

    public static Date convertStringToDate(String data) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            if (convertEmptyToNull(data) != null) {
                date = formatter.parse(data);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertDateToString(Date data) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(data);
    }

    public static String convertDateToString1(Date data) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return formatter.format(data);
    }

    public static Date getDateByFormat(String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format == null ? "yyyy-MM-dd" : format);
        Date date;
        try {
            date = formatter.parse(formatter.format(new Date()));
        } catch (ParseException e) {
            date = new Date();
            e.printStackTrace();
        }
        return date;
    }

    public static Triple<CellStyle, String, Object> tripleOf(CellStyle cellStyle, String columnName, Object value) {
        return Triple.of(cellStyle, columnName, value);
    }

    public static InputStream getFileTemplateFromPathResourceFolder(String fileName) {
        Resource resource = new ClassPathResource("template/" + fileName);
        try {
            return resource.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertIntToString(int number) {
        int dividend = number;
        StringBuilder columnName = new StringBuilder();
        int modulo;

        while (dividend > 0) {
            modulo = (dividend - 1) % 26;
            columnName.insert(0, (char) (65 + modulo));
            dividend = (dividend - modulo) / 26;
        }
        return columnName.toString();
    }

}
