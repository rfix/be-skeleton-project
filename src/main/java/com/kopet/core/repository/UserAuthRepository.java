package com.kopet.core.repository;

import com.kopet.core.entity.users.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {
  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);

  @Query(value = "SELECT * FROM USER_AUTH u WHERE u.username = :userName OR u.email = :userName ", nativeQuery = true)
  Optional<UserAuth> findByEmailOrUsername(String userName);

  @Query(value = "SELECT * FROM USER_AUTH u WHERE u.refreshToken = :refreshToken LIMIT 1 ", nativeQuery = true)
  Optional<UserAuth> findByRefreshToken(String refreshToken);
}
