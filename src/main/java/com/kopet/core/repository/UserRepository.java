package com.kopet.core.repository;

import com.kopet.core.constant.RoleConstant;
import com.kopet.core.entity.users.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    @Query(value = "SELECT * FROM USERS u " +
            "JOIN USER_AUTH ua ON ua.id = u.userAuthId " +
            "WHERE (:name IS NULL OR u.name LIKE %:name%) AND (:userId IS NULL OR u.userAuthId NOT IN (:userId)) AND ua.username <> 'sadmin'",
            countQuery = "SELECT COUNT(u.userAuthId) FROM USERS u " +
                    "JOIN USER_AUTH ua ON ua.id = u.userAuthId  " +
                    "WHERE (:name IS NULL OR u.name LIKE %:name%) AND (:userId IS NULL OR u.userAuthId NOT IN (:userId)) AND ua.username <> 'sadmin'",
            nativeQuery = true)
    Page<Users> getListUsers(Long userId, String name, Pageable pageable);

    @Query(value = "SELECT u.* FROM USER_RELATIONS ur JOIN USERS u ON u.userAuthId = ur.userParentId WHERE ur.userId = :userAuthId", nativeQuery = true)
    Optional<Users> getDataParent(Long userAuthId);

    @Query(value="SELECT u.* FROM ROLES r JOIN USER_ROLES ur ON ur.roleid = r.id JOIN USERS u ON u.userAuthId = ur.userAuthId " +
            "            WHERE r.type = '" + RoleConstant.STUDENT + "' AND (:name IS NULL OR u.name LIKE %:name%)",
            countQuery = "SELECT COUNT(u.userAuthId) FROM ROLES r JOIN USER_ROLES ur ON ur.roleid = r.id JOIN USERS u ON u.userAuthId = ur.userAuthId " +
                    "            WHERE r.type = '" + RoleConstant.STUDENT + "' AND (:name IS NULL OR u.name LIKE %:name%)",
            nativeQuery = true)
    Page<Users> getListStudents(String name, Pageable pageable);

    @Query(value = "SELECT u.userAuthId, u.numberPhone, u.dateOfBirth, u.name, u.grade, u.position, u.nrp, " +
            "p.loginAt, p.remarkLogin, d1.value AS flaggingPenaltyLogin, " +
            "p.logoutAt, p.remarkLogout, d2.value AS flaggingPenaltyLogout " +
            "FROM USER_RELATIONS ur " +
            "JOIN USERS u ON u.userAuthId = ur.userId " +
            "LEFT JOIN PRESENCES p ON p.userAuthId = u.userAuthId AND DATE(p.loginAt) = IFNULL(DATE(:date), CURDATE()) " +
            "LEFT JOIN DROPDOWNS d1 ON d1.key = p.keyLoginPenalty " +
            "LEFT JOIN DROPDOWNS d2 ON d2.key = p.keyLogoutPenalty " +
            "WHERE ur.userParentId = :userAuthId", nativeQuery = true)
    List<Map<String, Object>> getDataChild(Long userAuthId, Date date);

    @Query(value = "SELECT u.userAuthId FROM USER_RELATIONS ur JOIN USERS u ON u.userAuthId = ur.userId WHERE ur.userParentId = :userAuthId", nativeQuery = true)
    List<Long> getIdsChild(Long userAuthId);
}
