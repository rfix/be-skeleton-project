package com.kopet.core.config;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class Schedule {
    private String name;
    private boolean active;
    private List<String> crons;
    private List<Task> tasks;

    public Schedule() {
    }

    public Schedule(String name, boolean active, String cron) {
        this.name = name;
        this.active = active;
        this.crons = Collections.singletonList(cron);
    }

    public Schedule(String name, boolean active, List<String> crons) {
        this.name = name;
        this.active = active;
        this.crons = crons;
    }

    public boolean isTaskActive(String name) {
        return tasks.stream().filter(t -> t.getName().equals(name)).map(Task::isActive).findFirst().orElse(false);
    }

    @Data
    public static class Task {
        private String name;
        private boolean active;
    }
}
