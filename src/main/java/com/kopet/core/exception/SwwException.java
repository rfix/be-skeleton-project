package com.kopet.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class SwwException extends RuntimeException {
    public SwwException(String message) {
        super(String.format("%s", message));
    }
}
