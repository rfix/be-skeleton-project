package com.kopet.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(String resourceName) {
        super(String.format("username %s not found!", resourceName));
    }

    public NotFoundException(String resourceName, String fieldName) {
        super(String.format("%s not found with %s ", resourceName, fieldName));
    }

    public NotFoundException(String resourceName, Long id) {
        super(String.format("%s not found with id %d ", resourceName, id));
    }

    public NotFoundException(String resourceName, String field, String objId) {
        super(String.format("%s not found with %s %s ", resourceName, field, objId));
    }

    public NotFoundException() {
        super("Data not found");
    }
}
