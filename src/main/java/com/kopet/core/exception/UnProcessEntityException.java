package com.kopet.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class UnProcessEntityException extends RuntimeException {
    private String resourceName;

    public UnProcessEntityException(String resourceName) {
        super(String.format("%s", resourceName));
    }
}
