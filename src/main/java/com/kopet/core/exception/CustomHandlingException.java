package com.kopet.core.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

/**
 * @author rofiq
 */
@ControllerAdvice
public class CustomHandlingException {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleExceptions(NotFoundException exception, WebRequest webRequest) {
        ResponseException response = new ResponseException();
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Not found");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}
