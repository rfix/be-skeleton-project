package com.kopet.core.exception;

import com.kopet.core.constant.ErrorCodeConstant;
import com.kopet.core.constant.MessageConstant;
import com.kopet.core.utils.ConvertObjectToJson;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ThrowCustomAuthenticationError {
    public static void badCredential(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        response.getWriter().write(ConvertObjectToJson.create(new BaseException(
                ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT),
                HttpStatus.UNAUTHORIZED.value(),
                MessageConstant.BAD_REQUEST,
                MessageConstant.BAD_CREDENTIALS,
                ErrorCodeConstant.BAD_REQUEST_CREDENTIAL,
                request.getRequestURI()
        )));
    }

    public static void invalidAccessToken(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        response.getWriter().write(ConvertObjectToJson.create(new BaseException(
                ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT),
                HttpStatus.UNAUTHORIZED.value(),
                MessageConstant.UNAUTHORIZED,
                MessageConstant.INVALID_REFRESH_TOKEN,
                ErrorCodeConstant.UNAUTHORIZED_REFRESH_TOKEN,
                request.getRequestURI()
        )));
    }

    public static void invalidUserSessionToken(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        response.getWriter().write(ConvertObjectToJson.create(new BaseException(
                ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT),
                HttpStatus.UNAUTHORIZED.value(),
                MessageConstant.UNAUTHORIZED,
                MessageConstant.SESSION_TIME_OUT,
                ErrorCodeConstant.UNAUTHORIZED_SESSION,
                request.getRequestURI()
        )));
    }
}
