package com.kopet.core.service.users;

import com.kopet.core.security.UserDetail;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.entity.users.Users;
import com.kopet.core.repository.UserAuthRepository;
import com.kopet.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class UserRelationsServiceImpl implements UserRelationsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAuthRepository userAuthRepository;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public Users save(Users users, Long userParentId) {
        Users userParent = userService.getDataById(userParentId);
        return userParent;
    }

    @Override
    public List<Map<String, Object>> findSubordinate(Date date) {
        UserAuth userAuth = UserDetail.currentUser().getUsers();
        return userRepository.getDataChild(userAuth.getId(), date);
    }

    @Override
    public Users getUserParent(Long userId) {
        return userRepository.getDataParent(userId).orElse(null);
    }
}
