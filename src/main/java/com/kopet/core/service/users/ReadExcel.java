package com.kopet.core.service.users;

import com.kopet.core.constant.FileTypeConstant;
import com.kopet.core.utils.Helper;
import com.kopet.core.entity.users.Users;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReadExcel {

    public static boolean hasExcelFormat(MultipartFile file) {
        return FileTypeConstant.SHEET.equals(file.getContentType());
    }

    public List<Users> readExcel(InputStream is) throws IOException {
        List<Users> lstResult = new ArrayList<>();
        int startRow = 3;
        try (XSSFWorkbook wb = new XSSFWorkbook(is);) {
            XSSFSheet sheet = wb.getSheetAt(0);
            while (true) {
                XSSFRow row = sheet.getRow(startRow++);
                if (row == null) {
                    break;
                }
                Users user = new Users();
                int colIndex = 1;
                XSSFCell cell = row.getCell(colIndex++);
                DataFormatter formatter = new DataFormatter();
                String dataCell = Helper.convertEmptyToNull(formatter.formatCellValue(cell));
                if (dataCell == null) {
                    break;
                }
                String name = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String grade = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String nrp = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String position = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String dateBirth = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String birthPlace = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String noHp = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String address = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex++);
                String tufin = cell == null ? null : formatter.formatCellValue(cell);
                cell = row.getCell(colIndex);
                String birth = cell == null ? null : formatter.formatCellValue(cell);

                user.setDateOfBirth(Helper.convertStringToDate(birth));
                user.setName(name);
                user.setAddress(address);
                user.setNoHp(noHp);
                user.setDateOfBirth(Helper.convertStringToDate(dateBirth));
                lstResult.add(user);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lstResult;
    }

}
