package com.kopet.core.service.users;

import com.kopet.core.constant.MessageConstant;
import com.kopet.core.dto.UserParentDTO;
import com.kopet.core.exception.NotFoundException;
import com.kopet.core.payload.request.users.UserRequest;
import com.kopet.core.security.UserDetail;
import com.kopet.core.utils.Helper;
import com.kopet.core.entity.users.*;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.entity.users.Users;
import com.kopet.core.exception.SwwException;
import com.kopet.core.exception.UnauthorizedException;
import com.kopet.core.payload.request.ChangePasswordRequest;
import com.kopet.core.payload.response.PagingResponse;
import com.kopet.core.payload.response.UserResponse;
import com.kopet.core.repository.*;
import com.kopet.core.repository.RoleRepository;
import com.kopet.core.repository.UserAuthRepository;
import com.kopet.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserAuthRepository userAuthRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ReadExcel readExcel;

    @Autowired
    UserRelationsService relationsService;

    @Override
    public Users getDataById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException(Users.class.getSimpleName(), id));
    }

    private UserAuth getDataUserAuth(Long id) {
        return userAuthRepository.findById(id).orElseThrow(() -> new NotFoundException(UserAuth.class.getSimpleName(), id));
    }

    @Override
    public UserResponse showUsers() {
        UserAuth userAuth = UserDetail.currentUser().getUsers();
        UserParentDTO userParent = new UserParentDTO();
        Users parent = relationsService.getUserParent(userAuth.getId());
        if (parent != null) {
            userParent = new UserParentDTO(parent);
        }
        UserResponse response = new UserResponse();
        response.setUsers(userAuth.getUsers());
        response.setUserParent(userParent);
        response.setUserAuth(userAuth);
        response.setIsCheckIn(response.getFlaggingPenaltyLogin() != null);
        response.setIsCheckOut(response.getFlaggingPenaltyLogout() != null);
        return response;
    }

    @Override
    public UserAuth getUserAuth(String username) {
        return userAuthRepository.findByEmailOrUsername(username).
                orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
    }

    @Override
    public UserResponse updateData(UserRequest request) {
        Users requestUsers = request.getProfile();
        Users users = UserDetail.currentUser().getUsers().getUsers();
        users.setAddress(requestUsers.getAddress());
        users.setDateOfBirth(requestUsers.getDateOfBirth() == null ? users.getDateOfBirth() : requestUsers.getDateOfBirth());
        users.setFirstName(requestUsers.getFirstName());
        users.setLastName(requestUsers.getLastName());
        users.setName(requestUsers.getFirstName() + " " + requestUsers.getLastName());
        users.setSex(requestUsers.getSex());
        users.setNoHp(requestUsers.getNoHp());

        UserAuth userAuth = users.getUserAuth();
        if (Helper.convertEmptyToNull(request.getEmail()) != null) {
            userAuth.setEmail(request.getEmail() == null ? userAuth.getEmail() : request.getEmail());
        }
        userAuthRepository.save(userAuth);

        UserParentDTO userParent = new UserParentDTO();
        if (request.getUserParentId() != null) {
            Users parent = relationsService.save(users, request.getUserParentId());
            userParent = new UserParentDTO(parent);
        }
        UserResponse userResponse = new UserResponse();
        userResponse.setUsers(userRepository.save(users));
        userResponse.setUserParent(userParent);
        userResponse.setUserAuth(userAuth);
        return userResponse;
    }

    @Override
    public void changePassword(ChangePasswordRequest request) {
        if (Helper.convertEmptyToNull(request.getPassword()) != null) {
            if (Helper.convertEmptyToNull(request.getPassword()) != null) {
                UserAuth userAuth = UserDetail.currentUser().getUsers();
                userAuth.setPassword(encoder.encode(request.getPassword()));
                userAuthRepository.save(userAuth);
            }
        } else {
            throw new SwwException(MessageConstant.SWW);
        }
    }

    @Override
    public Users updateUser(Long id, Users request) {
        Users users = userRepository.findById(id).orElseThrow(() -> new NotFoundException(Users.class.getSimpleName(), id));
        users.setName(request.getName());
        users.setFirstName(request.getFirstName());
        users.setLastName(request.getLastName());
        users.setNoHp(request.getNoHp());
        users.setDateOfBirth(request.getDateOfBirth());
        users.setSex(request.getSex());
        return userRepository.save(users);
    }

//    @Transactional(rollbackFor = Exception.class)
//    private void saveListUsers(List<Users> usersList) {
//        for (Users users : usersList) {
//            Optional<UserAuth> checkUserAuth = userAuthRepository.findByEmailOrUsername(users.getNrp());
//            if (checkUserAuth.isPresent()) {
//                continue;
//            }
//            UserAuth userAuth = new UserAuth(users.getNrp(), null,
//                    encoder.encode(users.getDateOfBirth() == null ? "12345678" : Helper.convertDateToString1(users.getDateOfBirth())));
//            Set<Role> roles = new HashSet<>();
//            Role userRole = roleRepository.findByName(RoleEnumConstant.ROLE_USER)
//                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//            roles.add(userRole);
//            userAuth.setRoles(roles);
//            UserAuth rowUser = userAuthRepository.save(userAuth);
//            Users user = new Users();
//            user.setName(users.getName());
//            user.setUserAuth(rowUser);
//            user.setDateOfBirth(users.getDateOfBirth());
//            user.setAddress(users.getAddress());
//            user.setNoHp(users.getNoHp());
//            user.setTufin(users.getTufin());
//            userRepository.save(user);
//        }
//    }

    @Override
    public void save(MultipartFile file) {
        try {
            List<Users> listUsers = readExcel.readExcel(file.getInputStream());
//            saveListUsers(listUsers);
        } catch (IOException i) {
            throw new RuntimeException("failed to read excel data: " + i.getMessage());
        }
    }

    @Override
    public void resetPassword(String username) {
        UserAuth userAuth = getUserAuth(username);
        userAuth.setPassword(encoder.encode(Helper.convertDateToString1(userAuth.getUsers().getDateOfBirth())));
        userAuthRepository.save(userAuth);
    }

    @Override
    public UserAuth getUserByRefreshToken(String refreshToken) {
        return userAuthRepository.findByRefreshToken(refreshToken).orElseThrow(
                () -> new UnauthorizedException(MessageConstant.INVALID_REFRESH_TOKEN)
        );
    }


    @Override
    public PagingResponse findUser(String name, Pageable page) {
        UserAuth userAuth = UserDetail.currentUser().getUsers();
        Page<Users> pageResult = userRepository.getListUsers(UserDetail.isAdmin() ? null : userAuth.getId(), name, page);
        return new PagingResponse(pageResult);
    }

    @Override
    public PagingResponse findStudent(String name, Pageable page) {
        Page<Users> pageResult = userRepository.getListStudents(name, page);
        return new PagingResponse(pageResult);
    }
}
