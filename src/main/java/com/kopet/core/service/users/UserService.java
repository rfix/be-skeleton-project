package com.kopet.core.service.users;

import com.kopet.core.payload.request.users.UserRequest;
import com.kopet.core.entity.users.UserAuth;
import com.kopet.core.entity.users.Users;
import com.kopet.core.payload.request.ChangePasswordRequest;
import com.kopet.core.payload.response.PagingResponse;
import com.kopet.core.payload.response.UserResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface UserService {
    Users getDataById(Long id);

    UserAuth getUserAuth(String username);

    UserResponse updateData(UserRequest request);

    void save(MultipartFile file);

    void resetPassword(String username);

    UserAuth getUserByRefreshToken(String refreshToken);

    PagingResponse findUser(String name, Pageable page);

    PagingResponse findStudent(String name, Pageable page);

    UserResponse showUsers();

    void changePassword(ChangePasswordRequest request);

    Users updateUser(Long id, Users requset);
}
