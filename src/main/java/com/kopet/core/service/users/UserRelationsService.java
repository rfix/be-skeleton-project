package com.kopet.core.service.users;

import com.kopet.core.entity.users.Users;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public interface UserRelationsService {
    Users save(Users users, Long userParentId);

    List<Map<String, Object>> findSubordinate(Date date);

    Users getUserParent(Long userId);
}
