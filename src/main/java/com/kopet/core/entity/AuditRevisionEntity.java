package com.kopet.core.entity;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Entity;

@Entity(name = "_REV_INFO")
@RevisionEntity(AuditRevisionEntity.Listener.class)
public class AuditRevisionEntity extends DefaultRevisionEntity {

    private static final long serialVersionUID = 1L;

    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public static class Listener implements RevisionListener {
        @Override
        public void newRevision(Object revisionEntity) {
            AuditRevisionEntity auditedRevisionEntity = (AuditRevisionEntity) revisionEntity;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String userName = authentication != null ? authentication.getName() : "sys";
            auditedRevisionEntity.setUser(userName);
        }
    }
}
