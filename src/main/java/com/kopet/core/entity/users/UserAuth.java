package com.kopet.core.entity.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kopet.core.entity.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "USER_AUTH",
        uniqueConstraints = {@UniqueConstraint(columnNames = "username"), @UniqueConstraint(columnNames = "email")
        })

public class UserAuth extends GeneralEntity {

  @NotBlank
  @Size(max = 20)
  @Column(name = "username")
  private String username;

  @Size(max = 50)
  @Email
  @Column(name = "email")
  private String email;

  @NotBlank
  @Size(max = 120)
  @JsonIgnore
  private String password;

  @Column(name = "refreshToken")
  private String refreshToken;

  @Column(name = "logoutAt")
  private Date logoutAt;

  @Column(name = "loginAt")
  private Date loginAt;

  @Column(name = "isActive")
  private Boolean isActive;

  @OneToOne(mappedBy = "userAuth", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  @JsonIgnore
  private Users users;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "USER_ROLES",
          joinColumns = @JoinColumn(name = "userAuthId"),
          inverseJoinColumns = @JoinColumn(name = "roleId"))
  @JsonIgnore
  private Set<Role> roles = new HashSet<>();

  public UserAuth(String username, String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

  public UserAuth() {}

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }
}
