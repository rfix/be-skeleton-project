package com.kopet.core.entity.users;

import com.kopet.core.constant.RoleEnumConstant;

import javax.persistence.*;

@Entity
@Table(name = "ROLES")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private RoleEnumConstant name;

	public Role() {}

	public Role(RoleEnumConstant name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RoleEnumConstant getName() {
		return name;
	}

	public void setName(RoleEnumConstant name) {
		this.name = name;
	}
}