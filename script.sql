CREATE TABLE `USER_AUTH` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT '0',
  `loginAt` date NULL DEFAULT NULL,
  `logoutAt` date NULL DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ROLES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ROLES` (`name`, `type`, `createdAt`, `updatedAt`)
VALUES
	('ROLE_USER', 'USER', NOW(), NOW()),
	('ROLE_ATASAN', 'ATASAN', NOW(), NOW()),
	('ROLE_ADMIN', 'ADMIN', NOW(), NOW());

CREATE TABLE `USER_ROLES` (
  `userAuthId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userAuthId`,`roleId`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `ROLES` (`id`),
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`userAuthId`) REFERENCES `USER_AUTH` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `USERS` (
  `userAuthId` int(11) NOT NULL AUTO_INCREMENT,
  `numberPhone` varchar(16) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `firstName` varchar(30) DEFAULT NULL,
  `middleName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`userAuthId`),
  KEY `userAuthId` (`userAuthId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userAuthId`) REFERENCES `USER_AUTH` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `_REV_INFO` (
	`id` int NOT NULL,
	`timestamp` bigint DEFAULT NULL,
	`user` varchar(45) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=INNODB;

CREATE TABLE `hibernate_sequence` (
`next_val` bigint(20) DEFAULT NULL
) ENGINE=INNODB ;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES (1);

CREATE TABLE `_REV_USER_AUTH` (
	`id` int(11) NOT NULL,
	`rev` int(11) NOT NULL,
	`revtype` tinyint(4) DEFAULT NULL,
	`loginAt` datetime DEFAULT NULL,
	`logoutAt` datetime DEFAULT NULL,
	PRIMARY KEY (`id`,`rev`),
	KEY `rev` (`rev`),
	CONSTRAINT `_rev_user_auth_ibfk_1` FOREIGN KEY (`rev`) REFERENCES `_REV_INFO` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

ALTER TABLE `USERS`
ADD COLUMN `name` VARCHAR(255) NULL AFTER `address`,
ADD COLUMN `grade` VARCHAR(100) NULL AFTER `name`,
ADD COLUMN `position` VARCHAR(100) NULL AFTER `grade`,
ADD COLUMN `nrp` VARCHAR(100) NULL AFTER `position`,
ADD COLUMN `description` VARCHAR(255) NULL AFTER `nrp`;

ALTER TABLE `USERS`
ADD COLUMN `birthplace` VARCHAR(100) NULL AFTER `description`;

CREATE TABLE `USER_MOBILE_DEVICES` (
  `userAuthId` int NOT NULL AUTO_INCREMENT,
  `deviceId` varchar(255) DEFAULT NULL,
  `deviceModel` varchar(255) DEFAULT NULL,
  `fcmToken` varchar(255) NOT NULL DEFAULT '',
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`userAuthId`),
UNIQUE KEY `deviceId_deviceModel_UNIQUE` (`deviceModel`,`deviceId`),
KEY `userAuthId` (`userAuthId`),
CONSTRAINT `user_mobile_device_ibfk_1` FOREIGN KEY (`userAuthId`) REFERENCES `USER_AUTH` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `SCHEDULE_TIME` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`key` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`cronFormat` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`createdAt` DATETIME NULL DEFAULT NULL,
	`updatedAt` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB;

INSERT INTO `SCHEDULE_TIME` (`key`, `cronFormat`, `createdAt`, `updatedAt`) VALUES ('auto_logout', '0 30 23 ? * MON-FRI', NOW(), NOW());

CREATE TABLE `TAJUK` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(500) DEFAULT NULL,
	`content` VARCHAR(500) DEFAULT NULL,
	`createdBy` VARCHAR(100) DEFAULT NULL,
	`updatedBy` VARCHAR(100) DEFAULT NULL,
	`createdAt` datetime DEFAULT NULL,
	`updatedAt` datetime DEFAULT NULL,
	PRIMARY KEY (`id`));

CREATE TABLE `_REV_TAJUK` (
	`id` BIGINT(19) NOT NULL,
	`REV` INT(10) NOT NULL,
	`REVTYPE` TINYINT(3) NULL DEFAULT NULL,
	`createdAt` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`, `REV`) USING BTREE,
	INDEX `fk_rev_info_1` (`REV`) USING BTREE,
	CONSTRAINT `fk_rev_info_1` FOREIGN KEY (`REV`) REFERENCES `_REV_INFO` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)ENGINE=InnoDB;


CREATE OR REPLACE VIEW USER_DETAILS AS
	SELECT a.id, a.username, a.email, u.*, m.deviceId, m.deviceModel, m.fcmToken
	FROM USER_AUTH a
	JOIN USERS u ON u.userAuthId = a.id
	LEFT JOIN USER_MOBILE_DEVICES m ON m.userAuthId = a.id;

CREATE TABLE `DROPDOWNS` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`key` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`value` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`flagging` VARCHAR(25) NOT NULL COLLATE 'utf8_general_ci',
	`isActive` TINYINT(1) NULL DEFAULT '1',
	`remarks` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`updatedAt` DATETIME NOT NULL,
	`createdAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `key_UNIQUE` (`key`) USING BTREE
)ENGINE=InnoDB;

INSERT INTO `DROPDOWNS` (`key`, `value`, `flagging`, `isActive`, `remarks`, `updatedAt`, `createdAt`)
VALUES ('penalty0', 'hijau', 'penalty_login', 1, '00:00;09:00;0.0',NOW(), NOW()),
('penalty1', 'kuning', 'penalty_login', 1, '09:01;10:00;0.5',NOW(), NOW()),
('penalty2', 'merah', 'penalty_login', 1, '10:01;16:00;0.75',NOW(), NOW()),
('penalty3', 'hitam', 'penalty_login', 1, '3',NOW(), NOW()),
('penalty4', 'hijau', 'penalty_logout', 1, '15:01;23:59;0.0', NOW(), NOW()),
('penalty5', 'kuning', 'penalty_logout', 1, '14:01;15:00;0.5', NOW(), NOW()),
('penalty6', 'merah', 'penalty_logout', 1, '00:00;14:00;0.75', NOW(), NOW()),
('penalty_key_8', 'Transportasi', 'penalty_key', 1, 'issue terkait transportasi ke kantor', NOW(), NOW());

CREATE TABLE `PRESENCES` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`userAuthId` INT(10) NOT NULL,
	`keyLoginPenalty` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`loginPenalty` DECIMAL(10,2) NULL DEFAULT '0.00',
	`loginAt` DATETIME NULL DEFAULT NULL,
	`remarkLogin` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`keyLogoutPenalty` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`logoutPenalty` DECIMAL(10,2) NULL DEFAULT '0.00',
	`logoutAt` DATETIME NULL DEFAULT NULL,
	`remarkLogout` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `userAuthId` (`userAuthId`) USING BTREE,
	INDEX `keyLoginPenalty` (`keyLoginPenalty`) USING BTREE,
	INDEX `keyLogoutPenalty` (`keyLogoutPenalty`) USING BTREE
)ENGINE=InnoDB;

CREATE TABLE USER_RELATIONS (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	userParentId INT(11) NULL DEFAULT NULL,
	userId INT(11) NULL DEFAULT NULL,
	createdAt DATETIME NULL DEFAULT NULL,
	updatedAt DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (id) USING BTREE,
	UNIQUE INDEX UC_relation (userParentId, userId) USING BTREE,
	INDEX idx_usr_rla (userParentId, userId) USING BTREE,
	INDEX idx_child_id (userId) USING BTREE,
	CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `ROLES` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`userAuthId`) REFERENCES `USER_AUTH` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)ENGINE=InnoDB;

ALTER TABLE `USER_AUTH`
	ADD UNIQUE INDEX `email` (`email`),
	ADD UNIQUE INDEX `username` (`username`);

ALTER TABLE `USER_AUTH`
	ADD COLUMN `refreshToken` VARCHAR(255) NULL DEFAULT NULL AFTER `password`;

ALTER TABLE `TAJUK`
	ADD COLUMN `activeAt` DATE NULL DEFAULT NULL AFTER `content`;

ALTER TABLE `TAJUK`
	ADD COLUMN `flagging` VARCHAR(50) NULL DEFAULT NULL AFTER `activeAt`;

ALTER TABLE `PRESENCES`
ADD COLUMN `reasonLogin` VARCHAR(50) NULL DEFAULT NULL AFTER `loginPenalty`,
ADD COLUMN `reasonLogout` VARCHAR(50) NULL DEFAULT NULL AFTER `logoutPenalty`;

CREATE TABLE `WORKING_DAYS` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`userAuthId` INT(11) NOT NULL,
	`date` DATE DEFAULT NULL,
	`updatedBy` VARCHAR(100) DEFAULT NULL,
	`updatedAt` DATETIME NOT NULL,
	`createdAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `userAuthId` (`userAuthId`) USING BTREE
);

CREATE TABLE `CONFIRM_APP` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`userAuthId` INT(11) NOT NULL,
	`tajukId` INT(11) NOT NULL,
	`updatedAt` DATETIME NOT NULL,
	`createdAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `userAuthId` (`userAuthId`) USING BTREE,
	INDEX `confirm_app_ibfk_2` (`tajukId`) USING BTREE,
	CONSTRAINT `confirm_app_ibfk_1` FOREIGN KEY (`userAuthId`) REFERENCES `USERS` (`userAuthId`) ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT `confirm_app_ibfk_2` FOREIGN KEY (`tajukId`) REFERENCES `TAJUK` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=InnoDB;

ALTER TABLE `PRESENCES`
ADD COLUMN `updatedBy` VARCHAR(255) NULL DEFAULT NULL AFTER `remarkLogout`;

ALTER TABLE `USERS`
ADD COLUMN `tufin` INT NULL AFTER `userAuthId`;

INSERT INTO `USER_AUTH` (`email`, `username`, `password`, `refreshToken`, `isActive`, `loginAt`, `logoutAt`, `createdAt`, `updatedAt`)
VALUES (NULL, 'sadmin', '$2a$10$R2f1./u8jRQpnweTRNg2SuY3NoJcIUecbm.Yfu0uZmdmV4P6juuYe', '3f98a956-a3bc-43e4-8407-909358b33320', 1, '2022-04-05', NULL, NOW(), NOW());

INSERT INTO `USER_ROLES` (`userAuthId`, `roleId`)
VALUES (
	(SELECT id FROM USER_AUTH WHERE username = 'sadmin'),
	(SELECT id FROM ROLES WHERE `type` = 'ADMIN'))
);

ALTER TABLE `PRESENCES`
	ADD COLUMN `isLoginByPass` TINYINT(1) NULL DEFAULT '0' AFTER `remarkLogin`,
	CHANGE COLUMN `remarkLogout` `remarkLogout` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `logoutAt`,
	ADD COLUMN `isLogoutByPass` TINYINT(1) NULL DEFAULT '0' AFTER `remarkLogout`;

